# SuperFastPython.com
# example of using a semaphore
from time import sleep
from random import random
from threading import Thread
from threading import Semaphore
import math




# target function
# number of customers waiting
num_of_customer = 3
# number of barbers available (workers)
num_of_barbers = 1
# Note: You can also use lock/mutex method as below
def task(semaphore, number):
    # attempt to acquire the semaphore
    # with statement will acquire and release semaphore after the process is done.
    with semaphore:
        # process
        value = random()
        sleep(value)
        # report result
        print(f'Barber has spent {math.floor(value * 10)} time on customer {number} ')


# create a semaphore
semaphore = Semaphore(num_of_barbers)

for i in range(num_of_customer):
    customer = Thread(target=task, args=(semaphore, i))
    customer.start()

# wait for all workers to complete...