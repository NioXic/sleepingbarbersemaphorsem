import threading
import time
from random import random
import math
'''
in this lock/mutex semaphor, two customer work is running together
'''

mutex = threading.Lock()  # is equal to threading.Semaphore(1)

def customer1():
    while True:
        sleep_time = math.floor(random() * 10)
        time.sleep(sleep_time)
        mutex.acquire()
        print("it's customer 1 turn")
        mutex.release()
        print("it's customer 1 finished")

        print('barbers work finished in', sleep_time)
        print('\n')


def customer2():
    while True:
        sleep_time = math.floor(random() * 10)
        time.sleep(sleep_time)
        mutex.acquire()
        print("it's customer 2 turn")
        mutex.release()
        print("it's customer 2 finished")


        print('barbers work finished in', sleep_time)
        print('\n')

def customer3():
    while True:
        sleep_time = math.floor(random() * 10)
        time.sleep(sleep_time)
        mutex.acquire()
        print("it's customer 3 turn")
        mutex.release()
        print("it's customer 3 finished")

        print('barbers work finished in', sleep_time)
        print('\n')




t1 = threading.Thread(target=customer1).start()
t2 = threading.Thread(target=customer2).start()
t3 = threading.Thread(target=customer3).start()